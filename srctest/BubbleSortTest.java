import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import at.mar.algo.generator.DataGenerator;
import at.mar.algo.sorting.BubbleSort;
import at.mar.algo.sorting.SelectionSort;

public class BubbleSortTest {
	
	private BubbleSort bSort;

	@Before
	public void setUp() throws Exception {
		this.bSort = new BubbleSort();
	}

	@Test
	public void testSort() {
		int[] arr = DataGenerator.generateDataArray(100, 0 ,100);
		int[] sorted = bSort.sorting(arr);
		
		for(int i = 1; i < sorted.length; i++){
			if(sorted[i] < sorted[i-1]){
				fail("not sorted");
			}
		}
		
		assertTrue(true);
	}

}
