import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import at.mar.algo.generator.DataGenerator;
import at.mar.algo.sorting.BubbleSort;
import at.mar.algo.sorting.RadixSort;

public class RadixSortTest {
	
	private RadixSort rSort;

	@Before
	public void setUp() throws Exception {
		this.rSort = new RadixSort();
	}

	@Test
	public void testSort() {
		int[] arr = DataGenerator.generateDataArray(100, 0 ,100);
		int[] sorted = rSort.sorting(arr);
		
		for(int i = 1; i < sorted.length; i++){
			if(sorted[i] < sorted[i-1]){
				fail("not sorted");
			}
		}
		
		assertTrue(true);
	}
}
