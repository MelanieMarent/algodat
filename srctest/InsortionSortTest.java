import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.mar.algo.generator.DataGenerator;
import at.mar.algo.sorting.InsertionSort;

public class InsortionSortTest {
	
	private InsertionSort iSort;

	@Before
	public void setUp() throws Exception {
		this.iSort = new InsertionSort();
	}

	@Test
	public void testSort() {
		int[] arr = DataGenerator.generateDataArray(100, 0 ,100);
		int[] sorted = iSort.sorting(arr);
		
		for(int i = 1; i < sorted.length; i++){
			if(sorted[i] < sorted[i-1]){
				fail("not sorted");
			}
		}
		
		assertTrue(true);
	}

}
