import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.mar.algo.generator.DataGenerator;
import at.mar.algo.sorting.SelectionSort;

public class SelectionSortTest {
	
	private SelectionSort sSort;

	@Before
	public void setUp() throws Exception {
		this.sSort = new SelectionSort();
	}

	@Test
	public void testSort() {
		int[] arr = DataGenerator.generateDataArray(100, 0 ,100);
		int[] sorted = sSort.sorting(arr);
		
		int last = 0;
		for(int i = 1; i < sorted.length; i++){
			if(sorted[i] < sorted[i-1]){
				fail("not sorted");
			}
		}
		
		assertTrue(true);
	}

}
