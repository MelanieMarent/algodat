package at.mar.algo.generator;

import java.util.Random;

public class DataGenerator {
	
	
	public static int[] generateDataArray(int size){
		
		int[] arr = new int[size];
		
		Random r = new Random();
		for(int i = 0;i<size;i++){
			arr[i] = r.nextInt();
		}
		
		return arr;
	}
	
	public static int[] generateDataArray(int size, int min, int max){
		
		int[] arr = new int[size];
		
		Random r = new Random();
		for(int i = 0;i<size;i++){
			arr[i] = r.nextInt(max - min)+min;
		}		
		
		return arr;
	}
	
	public static void printArray(int[] data){
		
		for(int i = 0; i < data.length; i++){
			System.out.println(data[i]);
		}
	}
	
}
