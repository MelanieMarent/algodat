package at.mar.algo.generator;

import at.mar.algo.sorting.BubbleSort;
import at.mar.algo.sorting.InsertionSort;

public class DataTest {
	
	public static void main(String[] args){
		int[] arr = DataGenerator.generateDataArray(10, -10, 10);
		
		//DataGenerator.printArray(arr);
		
		/*InsertionSort iSort = new InsertionSort();
		int[] arrSorted = new int[arr.length];
		arrSorted = iSort.sorting(arr);
		DataGenerator.printArray(arrSorted);*/
		
		BubbleSort bs = new BubbleSort();
		int[] sorted = new int[arr.length];
		sorted = bs.sorting(arr);
		DataGenerator.printArray(sorted);
	}
	
}
