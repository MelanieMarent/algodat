package at.mar.algo.sorting;

public interface Sorting {
	public int[] sorting(int[] unsorted);
}
