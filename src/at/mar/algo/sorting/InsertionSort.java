package at.mar.algo.sorting;

public class InsertionSort implements Sorting{

	public int[] sorting(int[] unsorted) {
		
		for(int i = 1; i < unsorted.length; i++){
			int compareValue = unsorted[i];
			int j = i-1;
			while(j >= 0 && unsorted[j] > compareValue){
				unsorted[j+1] = unsorted[j];
				j--;
			}
			unsorted[j+1] = compareValue;  
		}
		
		return unsorted;
	}

}
