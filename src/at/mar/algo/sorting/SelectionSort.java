package at.mar.algo.sorting;

public class SelectionSort implements Sorting{

	@Override
	public int[] sorting(int[] unsorted) {
		
		for(int i = 0; i < unsorted.length - 1; i++){
            int min = i; 
            for (int j = i+1; j < unsorted.length; j++) 
                if (unsorted[j] < unsorted[min]) 
                	min = j; 
  
            int temp = unsorted[min]; 
            unsorted[min] = unsorted[i]; 
            unsorted[i] = temp; 
		}
		return unsorted;
	}

}
