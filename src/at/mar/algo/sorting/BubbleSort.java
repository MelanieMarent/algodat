package at.mar.algo.sorting;

public class BubbleSort implements Sorting{

	@Override
	public int[] sorting(int[] unsorted) {
		
		int first, second;
		boolean swapped = true;
		
		while(swapped == true){
			swapped = false;
			for(int i = 0; i < unsorted.length - 1; i++){
				if(unsorted[i] > unsorted[i+1]){
					first = unsorted[i];
					second = unsorted[i+1];
					
					unsorted[i] = second;
					unsorted[i+1] = first;
					swapped = true;
				}
			}
			
		}
		
		
		return unsorted;
	}

}
